package com.team.cn.enums;

import java.io.Serializable;

public enum UserErrorEnums implements IErrorCode, Serializable {
    EMPTY_PARAM("E-001","参数为空"),
    ERROR_PARAM("E-002","参数类型错误"),
    EMPTY_USER("100","用户为空"),
    ALREADY_LIKE("666","已经点过赞了");
    private String errorCode;
    private String errorMessage;

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public String toString() {
        return "UserErrorEnums{" +
                "errorCode='" + errorCode + '\'' +
                ", errorMessage='" + errorMessage + '\'' +
                '}';
    }

    UserErrorEnums(String errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    @Override
    public String getErrorCode() {
        return errorCode;
    }

    @Override
    public String getMessage() {
        return errorMessage;
    }
}
