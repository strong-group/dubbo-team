package com.team.cn.enums;

public interface IErrorCode {
    String getErrorCode();
    String getMessage();
}
