package com.team.cn.enums;

public enum GmEnum {
    TEL_LOGIN("loginAuthCode:"),
    TEL_REGISTER("registerAuthCode:"),
    TEL_BINDING("bindingAuthCode:"),
    LOGIN_TOKEN("userToken:");

    private String str;



    @Override
    public String toString() {
        return "GmEnum{" +
                "str='" + str + '\'' +
                '}';
    }

    GmEnum(String str) {
        this.str = str;
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }
}
