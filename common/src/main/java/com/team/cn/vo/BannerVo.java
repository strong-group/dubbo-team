package com.team.cn.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "轮播图")
public class BannerVo implements Serializable {

    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "轮播图id")
    private String bannerImgUrl;

    @ApiModelProperty(value = "路径")
    private String apiUrl;

}