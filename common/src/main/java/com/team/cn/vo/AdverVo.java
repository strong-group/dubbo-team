package com.team.cn.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "广告")
public class AdverVo implements Serializable {

    @ApiModelProperty(value = "广告id")
    private Integer id;

    @ApiModelProperty(value = "广告图片路径")
    private String adImgUrl;

    @ApiModelProperty(value = "广告商的网址")
    private String adUrl;

    @ApiModelProperty(value = "概率值")
    private Integer prNum;


}