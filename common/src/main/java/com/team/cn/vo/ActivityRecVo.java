package com.team.cn.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

@Data
@ApiModel(value = "活动推荐")
public class ActivityRecVo implements Serializable {

    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "活动标题")
    private String recommentTitle;

    @ApiModelProperty(value = "活动开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startTime;

    @ApiModelProperty(value = "活动结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endTime;

    @ApiModelProperty(value = "活动举办地点")
    private String holdPlace;

    @ApiModelProperty(value = "主办方")
    private String sponsor;

    @ApiModelProperty(value = "星期")
    private String weekday;

    @ApiModelProperty(value = "活动状态 0表示活动结束，1表示活动正在进行中")
    private Integer status;

    @ApiModelProperty(value = "活动状态描述")
    private String statusInfo;

    @ApiModelProperty(value = "举办城市")
    private Integer city;

    @ApiModelProperty(value = "活动介绍")
    private String activityIntroduce;
}

