package com.team.cn.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "导航栏信息")
public class NavbarVo implements Serializable {

    @ApiModelProperty(value = "导航栏id")
    private Integer id;
    @ApiModelProperty(value = "导航栏名字")
    private String name;
    @ApiModelProperty(value = "导航栏地址")
    private String url;
}