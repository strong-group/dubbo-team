package com.team.cn.service;

import com.alibaba.fastjson.JSONObject;
import com.team.cn.utils.ReturnResult;
import com.team.cn.vo.WxVo;

public interface WxService {
     ReturnResult add(JSONObject jsonObject2);

     ReturnResult<WxVo> queryWx( WxVo wxVo, String token);

     ReturnResult wxAndPhone(String tel, String authCode, WxVo wxVo);
}
