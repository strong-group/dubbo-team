package com.team.cn.service;

import com.team.cn.dto.LikingDto;
import com.team.cn.dto.MenuDetailsDto;

import java.util.List;

public interface ToMenuService {

    //根据mid查询list
    List<MenuDetailsDto> queryByMid(int mId);

    //根据mid和uid查询用户给哪些文章点赞，查文章的id
    List queryLike(String uId, int mId);

    //点赞
    void like(String uId, String aId, int num);

    //保存点赞到数据库
    void saveInDB(String uId, String aId,String mId);


}
