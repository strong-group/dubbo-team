package com.team.cn.service;


import com.alibaba.dubbo.config.annotation.Service;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.team.cn.dto.CourceComment;
import com.team.cn.dto.CourceCommentExample;
import com.team.cn.dto.User;
import com.team.cn.exception.UserBussinessException;
import com.team.cn.mapper.CourceCommentMapper;
import com.team.cn.utils.XssKillerUtil;
import com.team.cn.vo.CommentVo;
import com.team.cn.vo.UserVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CourceCommentMapper courceCommentMapper;


    /**
     * 分页查询
     *
     * @param pageSize
     * @param pageNo
     * @return
     */
    @Override
    public Map showCommentList(int pageNo, int pageSize) {
        CourceCommentExample courceCommentExample = new CourceCommentExample();
        courceCommentExample.setLimit(pageNo);
        courceCommentExample.setOffset(pageSize);
        Map<String, Object> maps = Maps.newHashMap();
        List<CourceComment> commentList = courceCommentMapper.selectByExample(courceCommentExample);
        List<CommentVo> voList = Lists.newArrayList();
        commentList.stream().forEach(dto -> {
            CommentVo vo = new CommentVo();
            BeanUtils.copyProperties(dto, vo);
            voList.add(vo);
        });
        long count = courceCommentMapper.countByExample(courceCommentExample);
        maps.put("commentList", voList);
        maps.put("count", count);
        return maps;
    }

    /**
     * 发表评论
     *
     * @param courceComment
     * @return
     */
    @Override
    public CourceComment comment(UserVo userVo, CourceComment courceComment) {
        this.filterContent(courceComment);
        courceComment.setuId(userVo.getId());
        courceComment.setHeadImg(userVo.getHeadImg());
        courceComment.setCommentTime(new Date());
        courceComment.setComment(courceComment.getComment());
        Assert.notNull(courceComment, "Comment不可为空！");
        courceCommentMapper.insertSelective(courceComment);
        return null;
    }

    /**
     * 回复评论
     *
     * @param courceComment
     * @return
     */
    @Override
    public void replyComment(UserVo UserVo,CourceComment courceComment) {
        /*HttpServletRequest request = null;
        HttpSession session = request.getSession();
        User loginUser = (User) session.getAttribute("LoginUser");*/
        courceComment.setuId(UserVo.getId());
        courceComment.setsId(courceComment.getsId());
        this.comment(UserVo,courceComment);
    }

    /**
     * 批量删除评论
     *
     * @param ids
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteCommentById(Integer... ids) {
        CourceCommentExample courceCommentExample = new CourceCommentExample();
        courceCommentExample.createCriteria().andIdIn(Arrays.asList(ids));
        return courceCommentMapper.deleteByExample(courceCommentExample) > 0;
    }

    /**
     * 点赞评论
     * @param id
     */
    @Override
    public void doSupport(int id) {

    }

    /**
     * 过滤评论内容
     *
     * @param comment
     */
    private void filterContent(CourceComment comment) {
        String content = comment.getComment();
        if (StringUtils.isEmpty(content) || "\n".equals(content)) {
            throw new UserBussinessException("说点什么吧");
        }
        // 过滤非法属性和无用的空标签
        if (!XssKillerUtil.isValid(content) || !XssKillerUtil.isValid(comment.getHeadImg())) {
            throw new UserBussinessException("请不要使用特殊标签");
        }
        content = XssKillerUtil.clean(content.trim()).replaceAll("(<p><br></p>)|(<p></p>)", "");
        if (StringUtils.isEmpty(content) || "\n".equals(content)) {
            throw new UserBussinessException("说点什么吧");
        }
        comment.setComment(content);
    }
}
