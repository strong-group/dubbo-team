package com.team.cn.conf;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("com.team.cn.mapper")
public class MybatisConfiguration {
}
