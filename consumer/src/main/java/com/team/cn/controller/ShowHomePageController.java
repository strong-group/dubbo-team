package com.team.cn.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.google.common.collect.Maps;
import com.team.cn.dto.MenuDetailsDto;
import com.team.cn.service.ShowHomePageService;
import com.team.cn.service.ToMenuService;
import com.team.cn.utils.ReturnResult;
import com.team.cn.utils.ReturnResultUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.Set;

@RestController
@Api(tags = "首页")
@RequestMapping(value = "/query")
public class ShowHomePageController {

    @Reference
    private ToMenuService toMenuService;
    @Reference
    private ShowHomePageService showHomePageService;

    @ApiOperation("查询首页信息")
    @GetMapping(value = "/index")
    public ReturnResult<Map<String, Object>> queryIndex(@RequestParam(name = "uId", required = false) String uId) {
        Map<String, Object> map = Maps.newHashMap();
        List navbarList = showHomePageService.queryNavbar();
        map.put("navbar", navbarList);
        Set set = showHomePageService.queryAdvertising();
        map.put("advertisement", set);
        List slideShowList = showHomePageService.querySlideShow();
        map.put("slideShow", slideShowList);
        List activityList = showHomePageService.queryActivity();
        map.put("activity", activityList);
        List courceList = showHomePageService.queryCource();
        map.put("cource", courceList);
        Map recommendMap = Maps.newHashMap();
        List<MenuDetailsDto> menuDetailsDtoList = toMenuService.queryByMid(1);
        recommendMap.put("recommendArticle", menuDetailsDtoList);
        if (!ObjectUtils.isEmpty(uId)) {
            List<Integer> LikeArticleList = toMenuService.queryLike(uId, 1);
            recommendMap.put("likeArticle", LikeArticleList);
        }
        map.put("recommend", recommendMap);
        return ReturnResultUtils.returnSuccess(map);
    }
}
