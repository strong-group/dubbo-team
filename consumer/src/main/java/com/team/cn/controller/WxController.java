package com.team.cn.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;

import com.team.cn.service.UserService;
import com.team.cn.service.WxService;
import com.team.cn.utils.HttpClientUtils;
import com.team.cn.utils.RedisUtils;
import com.team.cn.utils.ReturnResult;
import com.team.cn.vo.WxVo;
import com.team.cn.wx.WxConfig;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Controller
@Api(tags = "微信相关操作")
@RequestMapping("/wx")
public class WxController {

    @Autowired
    private WxConfig wxConfig;
    @Reference
    private WxService wxService;
    @Autowired
    private RedisUtils redisUtils;
    @Reference
    private UserService userService;

    //获取用户信息
    @GetMapping(value = "/loginWx")
    public String loginWx() {
        String codeUri = wxConfig.reqCodeUri();
        return "redirect:" + codeUri;
    }

    /**
     * 接受loginwx传递的值，最后返回的值是用户相关信息
     *
     * @param code
     * @throws IOException
     */
    @ResponseBody
    @GetMapping(value = "/callBack")
    public String callBack(String code) throws IOException {
        JSONObject jsonObject2 = null;
        if (null != code) {
            String accessToken = HttpClientUtils.doGet(wxConfig.reqAccessTokenUri(code));
            JSONObject jsonObject = JSONObject.parseObject(accessToken);
            String userInfo = HttpClientUtils.doGet(wxConfig.reqUserInfoUri(jsonObject.getString("access_token"), jsonObject.getString("openid")));
            jsonObject2 = JSONObject.parseObject(userInfo);
            redisUtils.set("wxFile:" + jsonObject2.getString("openid"), jsonObject2);
        }
        return jsonObject2.getString("openid");
    }

    /*
   微信注册
    */
    @ApiOperation("微信注册")
    @GetMapping(value = "/wxAdd")
    @ResponseBody
    public ReturnResult wxAdd(@RequestParam(name = "key") String key) {
        JSONObject jsonObject = (JSONObject) redisUtils.get("wxFile:" + key);
        return wxService.add(jsonObject);
    }

    /*
   微信登陆
    */
    @ApiOperation("微信登陆")
    @GetMapping(value = "/wxlogin")
    @ResponseBody
    public ReturnResult<WxVo> wxlogin(@RequestParam(name = "key") String key, HttpServletRequest request) {
        String jsonObject = redisUtils.get("wxFile:" + key).toString();
        String token = request.getSession().getId();
        WxVo wxVo = JSONObject.parseObject(jsonObject, WxVo.class);
        return wxService.queryWx(wxVo, token);
    }

    @ApiOperation("wx绑定发送验证码")
    @GetMapping(value = "/sentAuthCodeWx")
    @ResponseBody
    public ReturnResult sentAuthCodeWx(@RequestParam(name = "tel") String tel, @RequestParam(name = "type") String type) {
        return userService.sentAuthCodes(tel, type);
    }

    //绑定手机号码
    @ApiOperation("微信绑定手机号码")
    @GetMapping(value = "/wxAndPhone")
    @ResponseBody
    public ReturnResult wxAndPhone(@RequestParam(name = "tel") String tel, @RequestParam(name = "authCode") String authCode, @RequestParam(name = "key") String key) {
        String jsonObject = redisUtils.get("wxFile:" + key).toString();
        WxVo wxVo = JSONObject.parseObject(jsonObject, WxVo.class);
        return wxService.wxAndPhone(tel, authCode, wxVo);
    }

}
