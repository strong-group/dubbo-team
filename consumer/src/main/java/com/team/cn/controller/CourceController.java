package com.team.cn.controller;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.alibaba.dubbo.common.utils.StringUtils;
import com.alibaba.dubbo.config.annotation.Reference;
import com.google.common.collect.Maps;
import com.team.cn.conf.CurrentUser;
import com.team.cn.conf.LoginRedis;
import com.team.cn.service.CourceService;
import com.team.cn.utils.PageUtils;
import com.team.cn.utils.ReturnResult;
import com.team.cn.utils.ReturnResultUtils;
import com.team.cn.vo.ActivityRecVo;
import com.team.cn.vo.AttendVo;
import com.team.cn.vo.CourceVo;
import com.team.cn.vo.UserVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Api(tags = "活动")
@RestController
@RequestMapping(value = "/cource")
public class CourceController {

    @Reference
    private CourceService courceService;

    @ApiOperation(value = "课程模糊查询")
    @GetMapping(value = "/queryAll")
    public ReturnResult<PageUtils<List<CourceVo>>> queryAll(@RequestParam(name = "searchStr", required = false) String searchStr,
                                                            @RequestParam(name = "pageNo", defaultValue = "1", required = true) int pageNo,
                                                            @RequestParam(name = "pageSize", defaultValue = "12", required = true) int pageSize) {
        PageUtils pageUtils = new PageUtils();
        pageUtils.setPageNo(pageNo);
        pageUtils.setPageSize(pageSize);
        pageUtils.setPageNo(pageNo);
        Map<String, Object> maps = courceService.queryCource(searchStr, pageUtils.getPageNo(), pageSize);
        pageUtils.setCurrentList((List<CourceVo>) maps.get("courceVoList"));
        pageUtils.setTotalCount((long) maps.get("count"));
        pageUtils.setCurrentPage(pageNo);
        return ReturnResultUtils.returnSuccess(pageUtils);
    }

    @ApiOperation("活动首页")
    @GetMapping(value = "/postLoginActivity")
    public ReturnResult<Map<String, Object>> postLoginActivity(@RequestParam(name = "pageNo", defaultValue = "1", required = true) int pageNo,
                                                               @RequestParam(name = "pageSize", defaultValue = "5", required = true) int pageSize,
                                                               @RequestParam(name = "city", defaultValue = "0", required = false) int city,
                                                               @RequestParam(name = "uId", required = false) String uId) {
        Map<String, Object> resultMap = Maps.newHashMap();
        if (!ObjectUtils.isEmpty(uId)) {
            List attendList = courceService.queryAttend(uId);
            resultMap.put("attendList", attendList);
        }
        PageUtils pageUtils = new PageUtils();
        pageUtils.setPageNo(pageNo);
        pageUtils.setPageSize(pageSize);
        Map<String, Object> objectMap = courceService.postLoginActivity(pageUtils.getPageNo(), pageUtils.getPageSize(), city);
        resultMap.put("newActivityRecVoList", objectMap.get("newActivityRecVoList"));
        resultMap.put("oldActivityRecVoList", objectMap.get("oldActivityRecVoList"));
        pageUtils.setTotalCount((long) objectMap.get("count"));
        pageUtils.setCurrentPage(pageNo);
        resultMap.put("pageUtils", pageUtils);
        return ReturnResultUtils.returnSuccess(resultMap);
    }

    @ApiOperation("活动详情")
    @GetMapping(value = "/eventDetails")
    public ReturnResult<List<ActivityRecVo>> eventDetails(@RequestParam(name = "id") int id) {
        List event = courceService.eventDetails(id);
        return ReturnResultUtils.returnSuccess(event);
    }

    @LoginRedis
    @ApiOperation("立即报名和取消报名")
    @GetMapping(value = "/signedUp")
    public ReturnResult signedUp(@CurrentUser UserVo userVo,
                                 @RequestParam(name = "activaty") int activaty) {
        int uid = userVo.getId();
        List cList = courceService.queryAttendBy(uid, activaty);
        if (CollectionUtils.isNotEmpty(cList)) {
            courceService.cancelEnrollment(uid, activaty);
            return ReturnResultUtils.returnFail(222, "取消报名");
        } else {
            courceService.signedUp(uid, activaty);
            return ReturnResultUtils.returnSuccess("报名成功");
        }
    }

    @LoginRedis
    @ApiOperation("添加活动")
    @GetMapping(value = "/appActivity")
    public ReturnResult appActivity(@Validated ActivityRecVo activityRecVo) {
        return courceService.appActivity(activityRecVo);
    }
}
