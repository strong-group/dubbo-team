package com.team.cn.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.team.cn.conf.CurrentUser;
import com.team.cn.conf.LoginRedis;
import com.team.cn.enums.GmEnum;
import com.team.cn.service.UserService;
import com.team.cn.utils.RedisUtils;
import com.team.cn.utils.ReturnResult;
import com.team.cn.vo.ActivityRecVo;
import com.team.cn.vo.UserVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Set;


@RestController
@RequestMapping(value = "/user")
@Api(tags = "用户手机号码相关操作")
public class UserController {

    @Reference
    private UserService userService;
    @Autowired
    private RedisUtils redisUtils;

    /*
    发送验证码
     */
    @ApiOperation("发送手机验证码,type =1为注册发送验证码，2为登陆发送验证码，3为微信绑定手机号码发送验证码")
    @GetMapping(value = "/sentAuthCode")
    public ReturnResult sentAuthCode(@RequestParam(name = "tel") String tel,
                                     @RequestParam(name = "type") String type) {
        return userService.sentAuthCodes(tel, type);
    }

    /*
    手机号码注册用户
    tel:手机号码
    authCode：短信验证码
     */
    @ApiOperation("手机号码注册")
    @PostMapping(value = "/appByTel")
    public ReturnResult appByTel(@Validated UserVo userVo,
                                 @RequestParam(name = "authCode") String authCode) {
        return userService.appByTel(userVo, authCode);
    }

    /*
    手机号码登陆
     */
    @ApiOperation("手机号码登陆")
    @GetMapping(value = "/loginTel")
    public ReturnResult<UserVo> loginTel(@RequestParam(name = "tel") String tel,
                                         @RequestParam(name = "password", required = false) String password,
                                         @RequestParam(name = "authCode", required = false) String authCode,
                                         HttpServletRequest request) {
        return userService.loginByTel(tel, authCode, password, request.getSession().getId());
    }

    @LoginRedis
    @ApiOperation("退出")
    @GetMapping(value = "/exit")
    public void exit(@RequestParam(name = "key") String key) {
        userService.exit(key);
    }
}
