package com.team.cn.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.google.common.collect.Maps;
import com.team.cn.conf.CurrentUser;
import com.team.cn.conf.LoginRedis;
import com.team.cn.dto.LikingDto;
import com.team.cn.dto.MenuDetailsDto;
import com.team.cn.service.ToMenuService;
import com.team.cn.utils.RedisUtils;
import com.team.cn.utils.ReturnResult;
import com.team.cn.utils.ReturnResultUtils;
import com.team.cn.vo.UserVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@Api(tags = "左边菜单操作")
@RestController
@RequestMapping(value = "/menu")
public class ToMenuController {

    @Reference
    private ToMenuService toMenuService;
    @Autowired
    private RedisUtils redisUtils;

    @LoginRedis
    @ApiOperation("查询菜单模块")
    @GetMapping("/toMenu")
    public ReturnResult<Map<String, Object>> queryByMid(@RequestParam(name = "uId", required = false) String uId, @RequestParam(name = "mId") int mId) {
        Map<String, Object> map = Maps.newHashMap();
        List<MenuDetailsDto> menuDetailsDtoList = toMenuService.queryByMid(mId);
        map.put("article", menuDetailsDtoList);
        //判断用户是否登录，登录了则查一下哪些文章点赞了，否直接返回查询结果
        if (ObjectUtils.isEmpty(uId)) {
            return ReturnResultUtils.returnSuccess(map);
        } else {
            List<Integer> LikeArticleList = toMenuService.queryLike(uId, mId);
            map.put("likeArticleId", LikeArticleList);
            return ReturnResultUtils.returnSuccess(map);
        }
    }

    @LoginRedis
    @ApiOperation("点赞")
    @GetMapping("/like")
    public ReturnResult like(@CurrentUser UserVo userVo, @RequestParam(name = "aId") String aId, @RequestParam(name = "num") int num, @RequestParam(name = "mId") String mId) {
        String uId = userVo.getId().toString();
        if (redisUtils.checkFreq("like", 1, 5)) {
            toMenuService.like(uId, aId, num);
            toMenuService.saveInDB(uId, aId, mId);
            return ReturnResultUtils.returnResult();
        }
        return ReturnResultUtils.returnFail(400, "对不起，服务器繁忙！");
    }
}
