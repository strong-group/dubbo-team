package com.team.cn.utils;

import lombok.Data;

import java.io.Serializable;
import java.util.Collection;

@Data
public class PageUtils<T>  implements Serializable{
    //当前页11011002
    private int currentPage;
    //一页多少条
    private int pageSize;
    //返回对象
    private Collection<T> currentList;
    //总条数
    private long totalCount;
    //总页数
    private long totalPage = 0l;
    //页码,limit左边的数，值从多少条数据开始
    private int pageNo;

    public int getPageNo() {
        return pageNo * pageSize - pageSize;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public void setTotalPage(long totalPage) {
        this.totalPage = totalPage;
    }

    public long getTotalPage() {
        if (totalCount % pageSize == 0) {
            totalPage = totalCount / pageSize;
        } else {
            totalPage = totalCount / pageSize + 1;
        }
        return totalPage;
    }

}
